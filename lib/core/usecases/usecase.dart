import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../error/failures.dart';

// interface for all usecases
abstract class UseCase<Type, Params>{
    Future<Either<Failure, Type>> call(Params params);
}

// For any usecase that don't need params
class NoParams extends Equatable {}
