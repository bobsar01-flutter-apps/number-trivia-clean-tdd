import 'package:equatable/equatable.dart';

// not written in tdd cos nothing to test in abstract class
abstract class Failure extends Equatable {
  Failure([List properties = const<dynamic>[]]): super(properties);
}

// General failures
class ServerFailure extends Failure {}

class CacheFailure extends Failure {}