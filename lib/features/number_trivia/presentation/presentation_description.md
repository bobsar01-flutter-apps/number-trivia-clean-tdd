### 1. Presentation Layer

This layer comprises of the following folders:

- Pages: Where the pages/screens live
- Widgets: Custom widgets that we don't want to have directly inside the pages to prevent page clutter
- Presentation logic holders: **bloc** in this case (Doesn't have to be bloc, can go for other logic holders like ChangeNotifier or Stateful Widget(not recommended))
    - The logic holder should be as lean as possible - should delegate all work down to Domain layer (Use Cases)