### 2. Domain Layer

This layer contains the bulk of the code and should not be affected by the whims of changing data sources. It should be independent of the platform and of anything else in the app. 

It should consist of the following folders:
- usecases - executes core business logic
- entities - contains business objects
- repositories - lies between **Domain** and **Data** layers.

#### i. Use Cases

This contains classes which encapsulate all the business cases of a particular usecase in the app.
- Eg in our app: GetConcreteNoTrivia & GetRandomNoTrivia

#### ii. Entities
This contains the business objects without additional functionalities. Such functionalities would live inside *models* inside **Data** layer. (*see data layer for further explanation*)
- Eg in our app: Trivia Text & Trivia Number

#### iii. Repositories
On the edge between **Domain** and **Data** lies the repositories. This uses *dependency inversion* to allow for independence of the **Domain** layer. 

Dependency inversion in a nutshell means that we create an abstract (because dart doesn't support interfaces) *repository* which defines a "contract" of what the repository must do. We then depend on the repository "contract" in the **Domain**, knowing that the actual implementaion of the repository in **Data** will fullfill this contract.
- Eg in our app: RandomTrivia and ConcreteNoTrivia. 
    
The **Domain** layer simply does not care how the number trivia will be obtained, it just cares that it will be obtained. The **Data** layer will reveal how the data will be defined and gotten.

Repository in the **Data** layer will contain and implement the abstract class - will conform to the "contract" so **Domain** will not care what's going on behind the scene. **Domain** repository just knows what kind of data it will always receive from the **Data** repository.

