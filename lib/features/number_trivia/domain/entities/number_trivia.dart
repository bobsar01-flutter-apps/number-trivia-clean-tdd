import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

// Equatable allows for easy object-to-object comparison. Dart ordinarily doesn't mark 2 objects containing same data as equal unless they refer to the same location in memory.
// A lot of work goes into changing this (overriding equal operator, hashcode etc).
// Equatable handles these changes under the hood for us
class NumberTrivia extends Equatable {
  final String text;
  final int number;

  NumberTrivia({
    @required this.text,
    @required this.number,
  }) : super([text, number]);
}
