### 3. Data Layer

This final layer consists of the following folders:
- datasources (remote & local)
- repositories
- models

#### i. Datasources
 Contains underlying data used in the application. There's usually one datasource for fetching *remote* data from api and one for getting *local* data cached in device. 
- Remote datasource: Perfoms API requests to retrieve remote data. For eg in our app, we'll be making GET request to *numbersapi.com*
- Local datasource: This can also interact with underlying device's OS (for GPS location and such) and not just DB interactions. Could use SharedPreferences package to cache data locally.

Both datasources will be combined in the repositories which in our case, will perform some logic to check whether the user is online and if so, will prefer to get fresh data from the remote datasource. Otherwise, cache data will be used. A simple cache model of *cache once obtained from api remotely* will ensure there is data available for display even when offline

#### ii. Repositories

This is known as the brains of the datalayer. Implements the abstract classes from the **Domain** so it will fulfill the "contract" defined by the **Domain**. Operations could range from - when to cache, what to cache, when to get data from the datasources (eg offline mode needs retrieving data from local) etc.

Repositories output entities unlike datasources which output models. Transforming raw data/JSON requires conversion logic as we can only work with dart objects. 
Such JSON conversion logic/methods shouldn't go into *entities* as *entities* should be completely platform and data layer independent. 
We only change the **Data** layer when a data-related change occurs but **Domain** should remains untouched & unaffected by such a change. For instance, if datasource format is changed from JSON to XML, we don't want the change to affect the **Domain**. 

#### iii. Models

These are simple classes which extend *entities* and add some functionality on top of them. As they are a subclass of *entities*, they can be casted into simple entities and the *repository* will output the entities with no additional functionalities or fields added on top

In our app for eg - models will contain toJSON and fromJSON methods. In a more complex app functionality more custom methods and even fields can be defined - for eg, if you're storing something locally in sql-like DB and you need to have ID inside the DB stored inside the model, you can add a custom field to the model to represent the ID.