import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:numbertrivia_clean_tdd/features/number_trivia/domain/entities/number_trivia.dart';
import 'package:numbertrivia_clean_tdd/features/number_trivia/domain/repositories/number_trivia_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:numbertrivia_clean_tdd/features/number_trivia/domain/usecases/get_concrete_number_trivia.dart';

class MockNumberTriviaRepository extends Mock
    implements NumberTriviaRepository {}

void main() {
  GetConcreteNumberTrivia usecase;
  MockNumberTriviaRepository mockNumberTriviaRepository;

  //setup runs before anything else
  setUp(() {
    mockNumberTriviaRepository = MockNumberTriviaRepository();
    usecase = GetConcreteNumberTrivia(repository: mockNumberTriviaRepository);
  });

  final tNumber = 1;
  final tNumberTrivia = NumberTrivia(number: tNumber, text: 'test');
  test('should get trivia for the number from the repository', () async {
    // arrange - provide functionality to the mocked instance of te repo
    when(mockNumberTriviaRepository.getConcreteNumberTrivia(any))
        .thenAnswer((_) async => Right(tNumberTrivia));
    // act - return the result of calling the usecase (execute isn't implemented at first)
    final result = await usecase(Params(number: tNumber));

    // assert - as usecase just gets data from repository expect that the result of executing the usecase is Right(as above)
    expect(result, Right(tNumberTrivia));

    //verify that the getConcreteNumberTrivia repository was called with expected number
    //very useful as helps detect errors that occur when incorrect args are passed to the methods of the dependencies of the usecase or any other class
    verify(mockNumberTriviaRepository.getConcreteNumberTrivia(tNumber));

    //verify that no more interactions are happening on the repository as once execute is called, usecase shouldn't do anything more with the repository
    verifyNoMoreInteractions(mockNumberTriviaRepository);
  });
}
