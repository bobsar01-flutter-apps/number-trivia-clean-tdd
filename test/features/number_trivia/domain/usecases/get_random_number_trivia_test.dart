import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:numbertrivia_clean_tdd/core/usecases/usecase.dart';
import 'package:numbertrivia_clean_tdd/features/number_trivia/domain/entities/number_trivia.dart';
import 'package:numbertrivia_clean_tdd/features/number_trivia/domain/repositories/number_trivia_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:numbertrivia_clean_tdd/features/number_trivia/domain/usecases/get_random_number_trivia.dart';

class MockNumberTriviaRepository extends Mock
    implements NumberTriviaRepository {}

void main() {
  GetRandomNumberTrivia usecase;
  MockNumberTriviaRepository mockNumberTriviaRepository;

  //setup runs before anything else
  setUp(() {
    mockNumberTriviaRepository = MockNumberTriviaRepository();
    usecase = GetRandomNumberTrivia(mockNumberTriviaRepository);
  });

  final tNumberTrivia = NumberTrivia(number: 1, text: 'test');

  test('should get trivia from the repository', () async {
    // arrange - provide functionality to the mocked instance of te repo
    when(mockNumberTriviaRepository.getRandomNumberTrivia())
        .thenAnswer((_) async => Right(tNumberTrivia));
    // act - return the result of calling the usecase (execute isn't implemented at first)
    final result = await usecase(NoParams());

    // assert - as usecase just gets data from repository expect that the result of executing the usecase is Right(as above)
    expect(result, Right(tNumberTrivia));

    //verify that the getRandomNumberTrivia repository was called with no params
    //very useful as helps detect errors that occur when incorrect args are passed to the methods of the dependencies of the usecase or any other class
    verify(mockNumberTriviaRepository.getRandomNumberTrivia());

    //verify that no more interactions are happening on the repository as once execute is called, usecase shouldn't do anything more with the repository
    verifyNoMoreInteractions(mockNumberTriviaRepository);
  });
}
