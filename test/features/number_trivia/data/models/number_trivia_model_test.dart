import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:numbertrivia_clean_tdd/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:numbertrivia_clean_tdd/features/number_trivia/domain/entities/number_trivia.dart';

import '../../../../fixtures/fixture_reader.dart';

void main() {
  //Should extend entity class
  final tNumberTrivialModel =
      NumberTriviaModel(number: 1, text: 'Test text int.');
  final tNumberTrivialModelDouble =
      NumberTriviaModel(number: 1, text: 'Test text double.');

  test('should be a subclass of NumberTrivia entity', () async {
    //assert
    expect(tNumberTrivialModel, isA<NumberTrivia>());
  });

  group('fromJSON', () {
    test('should return a valid model when the JSON number is an integer', () {
      // arrange
      final Map<String, dynamic> jsonMap = json.decode(fixture('trivia.json'));

      // act
      final result = NumberTriviaModel.fromJSON(jsonMap);

      // assert
      expect(result, tNumberTrivialModel);
    });

    test('should return a valid model when the JSON number is a double', () {
      // arrange
      final Map<String, dynamic> jsonMap =
          json.decode(fixture('trivia_double.json'));

      // act
      final result = NumberTriviaModel.fromJSON(jsonMap);

      // assert
      expect(result, tNumberTrivialModelDouble);
    });
  });

  group('toJSON', () {
    test('should return a JSON map containing the proper data', () async {
      // act
      final result = tNumberTrivialModel.toJSON();

      final expectedMap = {
        "text": "Test text int.",
        "number": 1.0,
      };
      // assert
      expect(result, expectedMap);
    });
  });
}
